package com.uhk.pingflow;

import java.util.ArrayList;

public class MeasurementHolder {
	static ArrayList<Measurement> measurements = new ArrayList<Measurement>();
	static double ping = 0;
	static double locationx = 0;
	static double locationy = 0;
	static int networktype = 0;
	static double packetloss = 0;
	static String client = "";
	static int signal = 0;
	
	
	public static int getSignal() {
		return signal;
	}

	public static void setSignal(int signal) {
		MeasurementHolder.signal = signal;
	}

	public static ArrayList<Measurement> getMeasurements() {
		return measurements;
	}
	
	public static Double getAverage()
	{
		double avg = 0;
		for(Measurement m: measurements)
		{
			avg += m.getPing();
		}
		return avg / measurements.size();
	}
	public static double getPing() {
		return ping;
	}

	public static void setPing(double ping) {
		MeasurementHolder.ping = ping;
	}

	public static int getRunningtime() {
		return MeasurementHolder.measurements.size();
	}

	public static double getLocationx() {
		return locationx;
	}

	public static void setLocationx(double d) {
		MeasurementHolder.locationx = d;
	}

	public static double getLocationy() {
		return locationy;
	}

	public static void setLocationy(double d) {
		MeasurementHolder.locationy = d;
	}

	public static int getNetworktype() {
		return networktype;
	}

	public static void setNetworktype(int networktype) {
		MeasurementHolder.networktype = networktype;
	}

	public static double getPacketloss() {
		return packetloss;
	}

	public static void setPacketloss(double packetloss) {
		MeasurementHolder.packetloss = packetloss;
	}

	public static String getClient() {
		return client;
	}

	public static void setClient(String client) {
		MeasurementHolder.client = client;
	}

	public static void setMeasurements(ArrayList<Measurement> measurements) {
		MeasurementHolder.measurements = measurements;
	}
	
	
}
