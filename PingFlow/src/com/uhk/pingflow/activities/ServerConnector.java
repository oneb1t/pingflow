package com.uhk.pingflow.activities;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.uhk.pingflow.MeasurementHolder;
import com.uhk.pingflow.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Toast;

public class ServerConnector extends Activity {

	private Socket socket;

	private static final int SERVERPORT = 5000;
	private static final String SERVER_IP = "212.80.69.78";
	byte[] msg;
	private EditText edit;
	private CheckedTextView consolelog;
	private DataInputStream dataInputStream = null;
	String result;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.android_socket_client);		
		edit = (EditText) findViewById(R.id.Client);
		consolelog = (CheckedTextView) findViewById(R.id.consolelog);
		new Thread(new ClientThread()).start();
	}

	public void onSendClick(View view) {
		if(MeasurementHolder.getAverage() != null)
		{
		try {
			// here we make data packet to meet database columns
			// int ,int        ,double   ,double   ,int        ,int   ,int       ,varchar
			// ping,runningtime,locationx,locationy,networktype,signal,packetloss,client
			// this is MUCH faster than JSON (no need for column names)
			String str = 
			   "S '" +  Math.round(MeasurementHolder.getAverage())	// ping
			 + "','" + MeasurementHolder.getRunningtime()  		    // runningtime
			 + "','" + MeasurementHolder.getLocationx()             // locationx   
			 + "','" + MeasurementHolder.getLocationy()             // locationy 
			 + "','" + MeasurementHolder.getNetworktype()           // networktype
			 + "','" + MeasurementHolder.getSignal()                // signal
			 + "','" + MeasurementHolder.getPacketloss()            // packetloss
			 + "','" + edit.getText().toString() + "'";             // clientname
			
			
			PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),	true);
			Log.i("SOCKET",str);
			out.println(str);

    		Toast toast = Toast.makeText(getBaseContext(), "Sucessfully sended",Toast.LENGTH_SHORT);
    		toast.show();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		else
		{
			Log.i("FAIL", "something is not working");
		}
		}
	public void onReceiveClick(View view) {
		try {
		String str = "L";
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),	true);
		Log.i("SOCKET",str);
		Toast toast = Toast.makeText(getBaseContext(), "Data received",Toast.LENGTH_SHORT);
		toast.show();
		out.println(str);
	} catch (UnknownHostException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	
	public void onClearLog(View view) {
		consolelog.setText("");
	}

	class ClientThread implements Runnable {

		@Override
		public void run() {

			try {
				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
				socket = new Socket(serverAddr, SERVERPORT);
				 dataInputStream = new DataInputStream(socket.getInputStream());

				 while (true) 
				 {
					 if(dataInputStream.available() > 0){
						 msg = new byte[dataInputStream.available()];
						 dataInputStream.read(msg, 0, dataInputStream.available());
						 Log.i("READSOCKET",new String(msg));
		                   runOnUiThread(new Runnable() {
		                        @Override
		                        public void run() {
		                        	result = new String(msg);
		                        	consolelog.append(result);
		                        	// parse result
		                
		                        }
		                    });
					 }
				 }
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}
}