package com.uhk.pingflow.activities;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.uhk.pingflow.MeasurementHolder;
import com.uhk.pingflow.PingGetThread;
import com.uhk.pingflow.R;

public class PingFlow extends Activity {
	boolean runningping = true;
	PingGetThread pingthread;
	private LocationManager locationManager;
	private LocationProvider locationProvider;
	private Location lastKnownLocation;
	private WifiManager wifiManager;
	private TelephonyManager telephonyManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping_main);
        
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		//locationProvider = LocationManager.NETWORK_PROVIDER;
		locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
		Log.i("LOCATION", locationManager.toString());
		Log.i("LOCATION", locationManager.toString());
		// check if we have LocationProvider if not use 0,0 as location for save
		if (locationManager != null || locationProvider != null) {
			lastKnownLocation = locationManager.getLastKnownLocation(locationProvider.toString());
		}
		if (lastKnownLocation == null) {
			lastKnownLocation = new Location("nolocation");
		}
		MeasurementHolder.setLocationx(lastKnownLocation.getLatitude());
		MeasurementHolder.setLocationy(lastKnownLocation.getLongitude());

		Log.i("LOCATION", "" + lastKnownLocation.getLatitude() + " "
				+ lastKnownLocation.getLongitude());


		wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		
		if(wifiManager != null)  // wifi connected
		{
		    MeasurementHolder.setSignal(wifiManager.getConnectionInfo().getRssi());													// etc..
		    MeasurementHolder.setNetworktype(10);
		}
		if(telephonyManager != null) // control for flight mode or emulator
		{
			List<CellInfo> cellinfo = telephonyManager.getAllCellInfo();
			if(cellinfo != null && !cellinfo.isEmpty())
			{
				CellSignalStrengthGsm cellSignalStrengthGsm = ((CellInfoGsm) cellinfo.get(0)).getCellSignalStrength();
				MeasurementHolder.setSignal(cellSignalStrengthGsm.getDbm()); // safe signal info
				MeasurementHolder.setNetworktype(telephonyManager.getNetworkType()); // returns 2G/3G/EDGE
			}
		}
        

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }
    public void showgraph(View view)
    {
    	Intent graph = new Intent(getApplicationContext(),PingGraph.class);
    	startActivity(graph);
    }  
    public void showconnect(View view)
    {
    	Intent serverconnect = new Intent(getApplicationContext(),ServerConnector.class);
    	startActivity(serverconnect);
    } 
    public void showabout(View view)
    {
    	Intent about = new Intent(getApplicationContext(),About.class);
    	startActivity(about);
    } 
    public void showstatistics(View view)
    {
    	Intent statistics = new Intent(getApplicationContext(),Statistics.class);
    	startActivity(statistics);
    } 
    public void startping(View view) { 
    	Button btn = (Button) findViewById(R.id.startstopping);
    	if(runningping)
    	{
    		btn.setText("Stop Ping");
    		pingthread = new PingGetThread();
    		pingthread.start();
    		pingthread.parsepingtime("ping -W 2 google.com", false);
    		runningping = false;
    		Toast toast = Toast.makeText(getBaseContext(), "Ping started",Toast.LENGTH_SHORT);
    		toast.show();
    	}
    	else
    	{
    		btn.setText("Start Ping");
    		pingthread.stopparse();
    		runningping = true;

    		Toast toast = Toast.makeText(getBaseContext(), "Ping stopped",Toast.LENGTH_SHORT);
    		toast.show();
    	}
    	}
    }

