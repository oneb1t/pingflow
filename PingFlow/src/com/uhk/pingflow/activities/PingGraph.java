package com.uhk.pingflow.activities;

import org.achartengine.GraphicalView;

import com.uhk.pingflow.LineGraph;
import com.uhk.pingflow.Measurement;
import com.uhk.pingflow.MeasurementHolder;
import com.uhk.pingflow.R;

import android.app.Activity;
import android.os.Bundle;

public class PingGraph extends Activity {
    LineGraph linegraph;
	GraphicalView mChartView;
	Thread data;
	Measurement lastvalue = new Measurement(0, 0);
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linegraph = new LineGraph();
    	mChartView = linegraph.getView(this);
        setContentView(R.layout.activity_ping_graph);
        data = new Thread() {
			public void run()
			{
				while(true)
				{
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					int size = MeasurementHolder.getMeasurements().size();
					if(size > 0)
					{
						Measurement lastpoint = MeasurementHolder.getMeasurements().get(size -1);
						if(lastvalue !=  lastpoint)
						{
						    linegraph.addNewPoints(MeasurementHolder.getMeasurements().get(size - 1));
						    lastvalue = lastpoint;
						    mChartView.repaint();
						}
					}
				}
			}
		};
		data.start();
    }
	@Override
	protected void onStart() {
		super.onStart();
		setContentView(mChartView);
	}
	
	
}
