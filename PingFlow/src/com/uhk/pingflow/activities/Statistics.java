package com.uhk.pingflow.activities;

import com.uhk.pingflow.MeasurementHolder;
import com.uhk.pingflow.R;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;

public class Statistics extends Activity {
	private ListView statistics;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);
		statistics = (ListView) findViewById(R.id.statistics);
		 String[] codeLearnChapters = new String[] 
			{ 
				 "Ping average: " + MeasurementHolder.getAverage() + ""
				 ,"Ping count: " + MeasurementHolder.getRunningtime() + ""
				 ,"Location x: " + MeasurementHolder.getLocationx() + ""
				 ,"Location y: " + MeasurementHolder.getLocationy() + ""
				 ,"Signal: " + MeasurementHolder.getSignal() + ""
				 ,"Wifi/Phone: " + MeasurementHolder.getNetworktype()
		    };
		    ArrayAdapter<String> statisticsfield = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, codeLearnChapters);
		    statistics.setAdapter(statisticsfield);
		
	}

}