package com.uhk.pingflow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;

public class PingGetThread extends Thread {
	BufferedReader stdInput;
	int added = 1;
	Process p;
	Timer t;
    public void parsepingtime(String cmd, boolean sudo){
        try {
            if(!sudo)
                p= Runtime.getRuntime().exec(cmd);
            else{
                p= Runtime.getRuntime().exec(new String[]{"su", "-c", cmd});
            }
            stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            startperiodicparse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   public void startperiodicparse()
   {
  //Declare the timer
    t = new Timer();

    //Set the schedule function and rate
    t.scheduleAtFixedRate(new TimerTask() {
        @Override
        public void run() {
        	try {
        		String lastline = stdInput.readLine();
				Log.i("PING","" + lastline);
				addMeasurements(lastline);
				//viewg.repaint();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }    
    },
    //Set how long before to start calling the TimerTask (in milliseconds)
    0,
    //Set the amount of time between each execution (in milliseconds)
    200);
   }
   
	
	public synchronized void addMeasurements(String resource)
	{
		if (resource == null)
		{
			added++;
			Measurement pom = new Measurement(added,10000);
			MeasurementHolder.measurements.add(pom);
			Log.i("TIME","" + added);
			Log.e("PINGTIME","" + "PING FAIL");
		}
		else
		{			
		String delims = "[ ]+";
		String[] tokens = resource.split(delims);
		delims = "[=]+";
		char[] check = tokens[0].toCharArray();
		if(String.valueOf(check).equals("PING")) // ignore first line
			return;
		if(String.valueOf(check).equals("no"))
			return;
		if(tokens.length >= 7) // hack to skip first line with data about ping target
		{
			String[] time = tokens[7].split(delims);
			Double pingtime = Double.parseDouble(time[1]);		
			added++;
			Measurement pom = new Measurement(added,pingtime);
			MeasurementHolder.measurements.add(pom);
			Log.i("TIME","" + added);
			Log.i("PINGTIME","" + pingtime);
		}
		}
	}
	public void stopparse() {
		t.cancel();
		p.destroy();
	}
	
}
