package com.uhk.pingflow;

public class Measurement {
	double ping;
	int time;
	
	public Measurement(Integer time,double pingtime) {
		ping = pingtime;
		this.time = time;
	}
	public double getPing() {
		return ping;
	}
	public void setPing(double ping) {
		this.ping = ping;
	}
	public long getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
}
